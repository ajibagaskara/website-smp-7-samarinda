const myslider = document.querySelectorAll('.myslider'),
dot = document.querySelectorAll('.dot')

let counter = 1;
slidefun(counter);

let timer = setInterval(autoslide, 8000);
function autoslide() {
    counter += 1;
    slidefun(counter);
}

function plusSlides(n) {
    counter += n;
    slidefun(counter);
    resetTimer();
}

function currentSlide(n) {
    counter = n;
    slidefun(counter);
    resetTimer();
}

function resetTimer() {
    clearInterval(timer);
    timer = setInterval(autoslide, 8000);
}

function slidefun(n) {
    let i;
    for (i = 0; i < myslider.length; i++) {
        myslider[i].style.display = "none";
    }
    for (i = 0; i < dot.length; i++) {
        dot[i].classList.remove('active');
    }
    if (n > myslider.length) {
        counter = 1;
    }
    if (n < 1) {
        counter = myslider.length;
    }
    myslider[counter - 1].style.display = "block";
    dot[counter - 1].classList.add('active');
}

(function ($) {

    "use strict";

    $(".scroll-top").hide();
    $(window).on("scroll", function () {
        if ($(this).scrollTop() > 300) {
            $(".scroll-top").fadeIn();
        } else {
            $(".scroll-top").fadeOut();
        }
    });
    $(".scroll-top").on("click", function () {
        $("html, body").animate({
            scrollTop: 0,
        }, 700)
    });

    $('.my-news-ticker').AcmeTicker({
        type: 'typewriter',
        direction: 'right',
        speed: 50,
        controls: {
            prev: $('.acme-news-ticker-prev'),
            toggle: $('.acme-news-ticker-pause'),
            next: $('.acme-news-ticker-next')
        }
    });

    new WOW().init();

    $('.related-post-carousel').owlCarousel({
        loop: false,
        autoplay: true,
        autoplayHoverPause: true,
        autoplaySpeed: 1500,
        smartSpeed: 1500,
        margin: 30,
        mouseDrag: true,
        nav: true,
        dots: true,
        navText: ["<i class='fas fa-angle-left'></i>", "<i class='fas fa-angle-right'></i>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            768: {
                items: 1
            },
            992: {
                items: 2
            }
        }
    });


})(jQuery);