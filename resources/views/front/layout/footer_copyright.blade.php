<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="item">
                    <h2 class="heading">Alamat Sekolah</h2>
                    <div class="list-item">
                        <div class="left">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="right">
                            Jl. Kadrie Oening, Air Hitam, Kec. Samarinda Ulu, <br> 
                            Kota Samarinda, Kalimantan Timur 75243
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="item">
                    <h2 class="heading">Tautan Penting</h2>
                    <ul class="useful-links">
                        <li><a href="https://nisn.data.kemdikbud.go.id/index.php/Cindex/formcaribynama" target="_blank"  >NISN</a></li>
                        <li><a href="https://disdik.samarindakota.go.id/profil/" target="_blank" >Kemdikdub</a></li>
                        <li><a href="https://paspor-gtk.belajar.kemdikbud.go.id/casgpo/login?service=https%3A%2F%2Fgtk.belajar.kemdikbud.go.id%2Fauth%2Flogin" target="_blank">SIMPKB</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-4">
                <div class="item">
                    <h2 class="heading">Kontak</h2>
                    <div class="list-item">
                        <div class="left">
                            <i class="far fa-envelope"></i>
                        </div>
                        <div class="right">
                            {{ $admin_email }}
                        </div>
                    </div>
                    <div class="list-item">
                        <div class="left">
                            <i class="fab fa-whatsapp"></i>
                        </div>
                        <div class="right">
                            {{ $admin_phone }} Atau Klik <a href="https://wa.me/{{ $admin_phone }}?text=Assalamualaikum%20Mohon%20Izin%20Bertanya?" target="_blank"> WA </a><!--tambahkan icon whatsapp dan whatsapp me-->
                        </div>            
                    </div>
                    <!-- <ul class="social">
            <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
            <li><a href=""><i class="fab fa-instagram"></i></a></li>
        </ul> -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="copyright">
    Created By STIA DIGITAL TEKNO, 2022
</div>