<div class="top">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <ul>
                    <li class="today-text">Tanggal : <span id="date"></span> </li>
                    <li class="email-text">Email Admin: {{ $admin_email }}</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="heading-area">
    <div class="container">
        <div class="row">
            <div class="col-md-4 d-flex align-items-center" style="width: 80%;">
                <div class="logo">
                    <a href="javascript:void;">
                        <img src="{{ asset('uploads/'.$logo_school) }}" alt="">
                    </a>
                </div>
                <div class="align-items-center" style="margin-left: 10px;">
                    <h4>SMP NEGERI 7 SAMARINDA</h4>
                    <p>Kreatif Inovatif Prestasi</p>
                </div>
            </div>
        </div>
    </div>
</div>