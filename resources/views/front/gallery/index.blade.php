@extends('front.layout.master')

@section('content')
    <div class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Galeri</h2>
                    <nav class="breadcrumb-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Galeri</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content">
        <div class="container">
            <div class="photo-gallery">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="photo-thumb">
                            <img src="uploads/n1.jpg" alt="">
                            <div class="bg"></div>
                            <div class="icon">
                                <a href="{{ asset('uploads/n1.jpg') }}" class="magnific"><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="photo-caption">
                            <a href="">Haaland scores before going off injured in Dortmund win and it is very real</a>
                        </div>
                        <div class="photo-date">
                            <i class="fas fa-calendar-alt"></i> Feb 28, 2022
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="photo-thumb">
                            <img src="uploads/n2.jpg" alt="">
                            <div class="bg"></div>
                            <div class="icon">
                                <a href="uploads/n2.jpg" class="magnific"><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="photo-caption">
                            <a href="">Haaland scores before going off injured in Dortmund win and it is very real</a>
                        </div>
                        <div class="photo-date">
                            <i class="fas fa-calendar-alt"></i> Feb 28, 2022
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="photo-thumb">
                            <img src="uploads/n3.jpg" alt="">
                            <div class="bg"></div>
                            <div class="icon">
                                <a href="uploads/n3.jpg" class="magnific"><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="photo-caption">
                            <a href="">Haaland scores before going off injured in Dortmund win and it is very real</a>
                        </div>
                        <div class="photo-date">
                            <i class="fas fa-calendar-alt"></i> Feb 28, 2022
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="photo-thumb">
                            <img src="uploads/n4.jpg" alt="">
                            <div class="bg"></div>
                            <div class="icon">
                                <a href="uploads/n4.jpg" class="magnific"><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="photo-caption">
                            <a href="">Haaland scores before going off injured in Dortmund win and it is very real</a>
                        </div>
                        <div class="photo-date">
                            <i class="fas fa-calendar-alt"></i> Feb 28, 2022
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="photo-thumb">
                            <img src="uploads/n1.jpg" alt="">
                            <div class="bg"></div>
                            <div class="icon">
                                <a href="uploads/n1.jpg" class="magnific"><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="photo-caption">
                            <a href="">Haaland scores before going off injured in Dortmund win and it is very real</a>
                        </div>
                        <div class="photo-date">
                            <i class="fas fa-calendar-alt"></i> Feb 28, 2022
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="photo-thumb">
                            <img src="uploads/n2.jpg" alt="">
                            <div class="bg"></div>
                            <div class="icon">
                                <a href="uploads/n2.jpg" class="magnific"><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="photo-caption">
                            <a href="">Haaland scores before going off injured in Dortmund win and it is very real</a>
                        </div>
                        <div class="photo-date">
                            <i class="fas fa-calendar-alt"></i> Feb 28, 2022
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="photo-thumb">
                            <img src="uploads/n3.jpg" alt="">
                            <div class="bg"></div>
                            <div class="icon">
                                <a href="uploads/n3.jpg" class="magnific"><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="photo-caption">
                            <a href="">Haaland scores before going off injured in Dortmund win and it is very real</a>
                        </div>
                        <div class="photo-date">
                            <i class="fas fa-calendar-alt"></i> Feb 28, 2022
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="photo-thumb">
                            <img src="uploads/n4.jpg" alt="">
                            <div class="bg"></div>
                            <div class="icon">
                                <a href="uploads/n4.jpg" class="magnific"><i class="fas fa-plus"></i></a>
                            </div>
                        </div>
                        <div class="photo-caption">
                            <a href="">Haaland scores before going off injured in Dortmund win and it is very real</a>
                        </div>
                        <div class="photo-date">
                            <i class="fas fa-calendar-alt"></i> Feb 28, 2022
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
