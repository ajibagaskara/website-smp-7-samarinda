@extends('front.layout.master')

@section('content')
    <div class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>{{ $post_detail->post_title }}</h2>
                    <nav class="breadcrumb-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('news') }}">Berita</a></li>
                            <li class="breadcrumb-item"><a href="{{ route('category.post', $post_detail->category_id) }}">{{ $post_detail->nCategory->category_name}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">
                                {{ $post_detail->post_title }}
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
   
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-6">
                    <div class="featured-photo">
                        <img src="{{ asset('uploads/'.$post_detail->post_photo) }}" style="widht: 400px;" alt="Post Photo">
                    </div>
                    <div class="sub">
                        <div class="item">
                            <b><i class="fas fa-user"></i></b>
                            <a href="javascript:void;">{{ $user_data->name }}</a>
                        </div>
                        <div class="item">
                            <b><i class="fas fa-edit"></i></b>
                            <a href="javascript:void;">{{ $post_detail->nCategory->category_name }}</a>
                        </div>
                        <div class="item">
                            <b><i class="fas fa-clock"></i></b>
                            @php
                                $update = strtotime($post_detail->updated_at);
                                $post_date = date('d F, Y',$update) ;
                            @endphp
                            {{ $post_date }}
                        </div>
                        <div class="item">
                            <b><i class="fas fa-eye"></i></b>
                            {{ $post_detail->visitors }}
                        </div>
                    </div>
                    <div class="main-text">
                        {!! $post_detail->post_detail !!}
                    </div>
                    
                    <div class="related-news">
                        <div class="related-news-heading">
                            <h2>Berita Terkait</h2>
                        </div>
                        <div class="related-post-carousel owl-carousel owl-theme">
                            {{-- count how many related post from postcontroller --}}
                            {{-- if the related post more than 5, use owl-carousel to slide the related post --}}
                            @php $i = 0; @endphp
                            @foreach($related_post_array as $item)
                                @php $i++; @endphp
                                @if($i > 5)
                                    @break
                                @endif
                                @if($item->id == $post_detail->id)
                                    @continue
                                @endif
                                <div class="item">
                                    <div class="photo">
                                        <img src="{{ asset('uploads/'.$item->post_photo) }}" alt="Gambar Berita">
                                    </div>
                                    <div class="category">
                                        <span class="badge bg-success">{{$item->nCategory->category_name}}</span>
                                    </div>
                                    <h3><a href="{{ route('post-detail',$item->id)}}">{{ $item->post_title }}</a></h3>
                                    <div class="date-user">
                                        <div class="user">
                                            @if($item->author_id == 0)
                                                @php
                                                    $user_data = \App\Models\Admin::where('id',$item->admin_id)->first();
                                                @endphp
                                            @else
                                                @php
                                                    $user_data = \App\Models\Author::where('id',$item->author_id)->first();
                                                @endphp
                                            @endif
                                            <a href="javascript:void">{{ $user_data->name }}</a>
                                        </div>
                                        <div class="date">
                                            @php
                                                $update = strtotime($item->updated_at);
                                                $post_date = date('d F, Y',$update) ;
                                            @endphp
                                            {{ $post_date }}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 sidebar-col">
                    <div class="sidebar">
                        <div class="widget">
                            <div class="tag-heading">
                                <h2>Tautan Penting</h2>
                            </div>
                            <div class="tag">
                                <div class="tag-item">
                                    <ul class="useful-links" style="padding-left: 0px;" >
                                        
                                        <li style="list-style-type: none; font-size: 1.3em;"><a href="https://nisn.data.kemdikbud.go.id/index.php/Cindex/formcaribynama" style="color: black; font-size:1em;" target="_blank" ><i class="fas fa-chevron-right" style="margin-right:0.1em; "></i>  NISN</a></li>

                                        <li style="list-style-type: none; font-size: 1.3em;"><a href="https://disdik.samarindakota.go.id/profil/" style="color: black; font-size:1em;" target="_blank"><i class="fas fa-chevron-right" style="margin-right:0.1em; "></i>  Kemdikdub</a></li>
                                            
                                        <li style="list-style-type: none; font-size: 1.3em;"><a href="https://paspor-gtk.belajar.kemdikbud.go.id/casgpo/login?service=https%3A%2F%2Fgtk.belajar.kemdikbud.go.id%2Fauth%2Flogin" style="color: black; font-size:1em;"target="_blank" > <i class="fas fa-chevron-right" style="margin-right:0.1em; "></i>  SIMPKB</a></li>
                                   
                                    </ul>
                                </div>
                            </div>
                        </div>      
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection