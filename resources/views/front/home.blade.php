@extends('front.layout.master')

@section('content')
   
    {{-- contain news ticker --}}
    <div class="news-ticker-item">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if($setting->news_ticker_status == 'Show')
                        <div class="acme-news-ticker">
                            <div class="acme-news-ticker-label">Pengumuman</div>
                            <div class="acme-news-ticker-box">
                                <ul class="my-news-ticker">
                                    @if($posts->isEmpty())
                                        <p>Belum Ada Pengumuman</p>
                                    @else
                                        @php $i=0; @endphp
                                        @foreach($posts as $item)
                                            @php $i++; @endphp
                                            @if($i > $setting->news_ticker_total)
                                                @break
                                            @endif
                                            <li><a href="{{ route('post-detail',$item->id)}}">{{ $item->post_title }}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    
    {{-- contain slider news --}}
    <div class="home-main">
        <div class="container">
            <div class="slider">
                @if($post_slider->isEmpty())
                    <div class="myslider fade" style="display:block;">
                        <div class="text">
                            <h2>Silahkan Isi / Aktifkan Slider Berita</h2>
                            <p>Silahkan Isi Berita</p>
                            <a href="javascript:void" class="btn btn-info btn-sm" style="color: #fff !important;">Selengkapnya</a>
                        </div>
                        <img src="{{ asset('uploads/school-placeholder.jpg') }}">
                    </div>
                @else
                    @php $i = 0; @endphp
                    @foreach($post_slider as $slider)
                    
                    @php $i++; @endphp
                    @if($i > 3)
                        @break
                    @endif
                    <div class="myslider fade" style="display:block;">
                        <div class="text">
                            <h2>{{$slider->post_title}}</h2>
                            <p>{{$slider->nCategory->category_name}}</p>
                            <a href="{{ route('post-detail',$slider->id) }}" class="btn btn-info btn-sm" style="color: #fff !important;">Selengkapnya</a>
                        </div>
                        <img src="{{ asset('uploads/'.$slider->post_photo) }}">
                    </div>
                    @endforeach
                @endempty

                <a class=" prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a>

                <div class="dotbox" style="text-align: center;">
                    <span class="dot" onclick="currentSlide(1)"></span>
                    <span class="dot" onclick="currentSlide(2)"></span>
                    <span class="dot" onclick="currentSlide(3)"></span>
                </div>
            
            </div>
        </div>
    </div>

    <div class="home-content">
        <div class="container">
            <div class="row">
                
                <div class="col-lg-9 col-md-6 left-col">
                    <div class="left">                            
                        <!-- News Of Category -->
                        <div class="news-total-item">
                            <div class="row">
                                <div class="col-lg-6 col-md-12">
                                    <h2>Berita Terbaru</h2>
                                </div>
                                <div class="col-lg-6 col-md-12 see-all">
                                    <a href="{{ route('news') }}" class="btn btn-primary btn-sm">Semua Berita</a>
                                </div>
                                <div class="col-md-12">
                                    <div class="bar"></div>
                                </div>
                            </div>
                                <div class="row">
                                    
                                    <div class="col-lg-6 col-md-12">
                                        @php $i = 0; @endphp
                                        @foreach($post_data as $item) 
                                        @php $i++; @endphp
                                        @if($i > 1)
                                            @break
                                        @endif
                                        <div class="left-side">
                                            <div class="photo mb-2">
                                                <img src="{{ asset('uploads/'.$item->post_photo)}}" alt="Gambar Berita">
                                            </div>
                                            <div class="category">
                                                <span class="badge bg-success">{{ $item->nCategory->category_name}}</span>
                                            </div>
                                            <h3><a href="{{ route('post-detail', $item->id )}}">{{$item->post_title}}</a></h3>
                                            <div class="date-user">
                                                <div class="user">
                                                    @if($item->author_id == 0)
                                                        @php
                                                            $user_data = \App\Models\Admin::where('id',$item->admin_id)->first();
                                                        @endphp
                                                    @else
                                                        @php
                                                            $user_data = \App\Models\Author::where('id',$item->author_id)->first();
                                                        @endphp
                                                    @endif
                                                    {{-- Cek jika tidak ada data author  --}}
                                                    @if($user_data == null)
                                                        <a href="#">Penulis Berita</a>
                                                    @else
                                                        <a href="#">{{ $user_data->name }}</a>
                                                    @endif
                                                </div>
                                                <div class="date">
                                                    @php
                                                        $update = strtotime($item->updated_at);
                                                        $post_date = date('d F, Y',$update) ;
                                                    @endphp
                                                    {{ $post_date }}
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="col-lg-6 col-md-12">
                                        @php $i = 0; @endphp
                                        @foreach($post_data as $item) 
                                        @php $i++; @endphp
                                        @if($i == 1)
                                            @continue
                                        @endif
                                        @if($i > 5)
                                            @break
                                        @endif
                                        <div class="right-side">
                                            <div class="right-side-item mb-2">
                                                <div class="left">
                                                    <img src="{{ asset('uploads/'.$item->post_photo)}}" alt="Gambar Berita">
                                                </div>
                                                <div class="right">
                                                    <h2><a href="{{ route('post-detail', $item->id )}}">{{$item->post_title}}</a></h2>
                                                    <div class="category">
                                                        <span class="badge bg-success">{{ $item->nCategory->category_name}}</span>
                                                    </div>
                                                    <div class="date-user">
                                                        <div class="user">
                                                            @if($item->author_id == 0)
                                                                @php
                                                                    $user_data = \App\Models\Admin::where('id',$item->admin_id)->first();
                                                                @endphp
                                                            @else

                                                            @endif
                                                            <a href="">{{ $user_data->name }}</a>
                                                        </div>
                                                        <div class="date">
                                                            @php
                                                                $update = strtotime($item->updated_at);
                                                                $post_date = date('d F, Y',$update) ;
                                                            @endphp
                                                            {{ $post_date }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            
                        </div>   
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 sidebar-col">
                    <div class="sidebar">

                        <div class="widget">
                            <div class="tag-heading">
                                <h2>Kategori Berita</h2>
                            </div>
                            <div class="tag">
                                <div class="tag-item">
                                    @foreach($categories as $item)
                                        <a href="{{ route('category.post',$item->id) }}"><span class="badge bg-secondary">{{ $item->category_name }}</span></a>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="widget">
                            <div class="tag-heading">
                                <h2>Tautan Penting</h2>
                            </div>
                            <div class="tag">
                                <div class="tag-item">
                                    <ul class="useful-links" style="padding-left: 0px;" >
                                        
                                        <li style="list-style-type: none; font-size: 1.3em;"><a href="https://nisn.data.kemdikbud.go.id/index.php/Cindex/formcaribynama" style="color: black; font-size:1em;" target="_blank" ><i class="fas fa-chevron-right" style="margin-right:0.1em; "></i>  NISN</a></li>

                                        <li style="list-style-type: none; font-size: 1.3em;"><a href="https://disdik.samarindakota.go.id/profil/" style="color: black; font-size:1em;" target="_blank"><i class="fas fa-chevron-right" style="margin-right:0.1em; "></i>  Kemdikdub</a></li>
                                            
                                        <li style="list-style-type: none; font-size: 1.3em;"><a href="https://paspor-gtk.belajar.kemdikbud.go.id/casgpo/login?service=https%3A%2F%2Fgtk.belajar.kemdikbud.go.id%2Fauth%2Flogin" style="color: black; font-size:1em;"target="_blank" > <i class="fas fa-chevron-right" style="margin-right:0.1em; "></i>  SIMPKB</a></li>
                                   
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="widget">
                            
                            <div class="live-channel">
                                <div class="live-channel-heading">
                                    <h2>
                                        <a href="{{ route('profil.sambutan-kepsek') }}" style="color: inherit;">
                                            Sambutan Kepala Sekolah
                                        </a>
                                    </h2>
                                </div>
                                <img src="{{ asset('uploads/'.$message->msg_photo)}}" style="width:100%; height:280px; object-fit:cover;" alt="Gambar Kepala Sekolah">                    
                                <div style="font-size: medium;">
                                    <u>{{ $message->name }}</u>
                                    <br>
                                    <span>{{ $message->position }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection