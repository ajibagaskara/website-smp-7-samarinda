@extends('front.layout.master')

@section('content')
    <div class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Sambutan Kepala Sekolah</h2>
                    <nav class="breadcrumb-container">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('home') }}">Beranda</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Sambutan Kepala Sekolah</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <div class="poll-item">
                        <div class="question">
                            Sambutan Kepala Sekolah
                        </div>
                        <div class="poll-date">
                            {{-- {!! $post_detail->post_detail !!} --}}
                            {!! $kalmtSambutan !!}
                        </div>
                    </div>
                    

                </div>
            </div>
        </div>
    </div>
@endsection
