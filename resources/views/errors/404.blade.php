<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">

    <link rel="icon" type="image/png" href="uploads/favicon.png">

    <title>Halaman Akses Administrator</title>

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@400;600;700&display=swap" rel="stylesheet">

    @include('admin.layout.head')
</head>

<body>
<div id="app">
    <div class="main-wrapper">
        <section class="section">
            <div class="container container-login">
                <div class="row">
                    <div class="col-xl-10 mx-auto">
                        <div class="card card-primary border-box">
                            <div class="card-header card-header-auth">
                                <h3 class="text-center">HALAMAN TIDAK DITEMUKAN</h3>
                                <h2 class="text-center">SILAHKAN KEMBALI KE BERANDA</h2>
                            </div>
                            <div class="card-body text-center">
                                <a href="{{ route('home') }}" class="btn btn-primary"><i class="fas fa-home"></i> Halaman Beranda </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<script src="{{ asset('dist/js/scripts.js')}}"></script>
<script src="{{ asset('dist/js/custom.js')}}"></script>

</body>
</html>