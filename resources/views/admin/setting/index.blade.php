@extends('admin.layout.master')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Pengaturan</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-xl-2 col-lg-3 col-md-4 col-sm-12">
                                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <a class="nav-link active" id="v-1-tab" data-toggle="pill" href="#v-1" role="tab" aria-controls="v-1" aria-selected="true">
                                                Pengumuman Beranda
                                            </a>
                                            <a class="nav-link" id="v-2-tab" data-toggle="pill" href="#v-2" role="tab" aria-controls="v-2" aria-selected="false">
                                                Sambutan Kepala Sekolah
                                            </a>
                                            <a class="nav-link" id="v-3-tab" data-toggle="pill" href="#v-3" role="tab" aria-controls="v-3" aria-selected="false">
                                                Logo Sekolah
                                            </a>
                                            {{-- <a class="nav-link" id="v-4-tab" data-toggle="pill" href="#v-4" role="tab" aria-controls="v-4" aria-selected="false">
                                                Tautan Penting
                                            </a> --}}
                                        </div>
                                    </div>
                                    <div class="col-xl-10 col-lg-9 col-md-8 col-sm-12">
                                        <div class="tab-content" id="v-pills-tabContent">
                                            
                                            {{-- pengaturan pengumuman --}}
                                            <div class="pt_0 tab-pane fade show active" id="v-1" role="tabpanel" aria-labelledby="v-1-tab">
                                                <form action="{{ route('setting.update') }}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                    <!-- News Ticker Start -->
                                                    <div class="form-group mb-3">
                                                        <label>Banyak Berita</label>
                                                        <input type="text" class="form-control" name="news_ticker_total" value="{{$setting->news_ticker_total}}">
                                                    </div>
                                                    <div class="form-group mb-3">
                                                        <label>Status Pengumuman</label>
                                                        <select name="news_ticker_status" class="form-control"> 
                                                            <option value="Show" @if($setting->news_ticker_status == 'Show') selected @endif>Tampilkan</option>
                                                            <option value="Hide" @if($setting->news_ticker_status == 'Hide') selected @endif>Tidak</option>
                                                        </select>
                                                    </div>
                                                    <!-- News Ticker End -->
                                                    <div class="form-group mt_30">
                                                        <button type="submit" class="btn btn-primary">Perbarui</button>
                                                    </div>
                                                </form>
                                                
                                            </div>
                                            
                                            {{-- pengaturan sambutan kepala sekolah --}}
                                            <div class="pt_0 tab-pane fade" id="v-2" role="tabpanel" aria-labelledby="v-2-tab">
                                                <form action="{{ route('setting.message') }}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                    <!-- Sambutan Start -->
                                                    <div class="form-group mb-3">
                                                        <label>Nama Kepala Sekolah</label>
                                                        <input type="text" class="form-control" name="name" value="{{$message->name}}">
                                                    </div>
                                                    <div class="form-group mb-3">
                                                        <label>Kalimat Sambutan</label>
                                                        <textarea name="msg" class="form-control" cols=50" rows="10" style="height:20%;">{{$message->msg}}</textarea>
                                                    </div>
                                                    <div class="form-group mb-3">
                                                        <label>Jabatan</label>
                                                        <input type="text" class="form-control" name="position"
                                                        value="{{$message->position}}">
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><b>Gambar Berita</b></label>
                                                            <img src="{{asset('uploads/'.$message->msg_photo)}}" alt="Gambar Berita" style="width:300px; display:block;">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Ganti Gambar Berita</b></label>
                                                            <input type="file" class="form-control" name="msg_photo">
                                                        </div>
                                                    </div>
                                                    <!-- Sambutan End -->
                                                    <div class="form-group mt_30">
                                                        <button type="submit" class="btn btn-primary">Perbarui</button>
                                                    </div>
                                                </form>
                                            </div>

                                            <div class="pt_0 tab-pane fade" id="v-3" role="tabpanel" aria-labelledby="v-3-tab">
                                                <form action="{{ route('setting.logo') }}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <label><b>Logo Sekolah</b></label>
                                                            <img src="{{asset('uploads/'.$logo->logo)}}" alt="Gambar Berita" style="width:300px; display:block;">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label><b>Ganti Logo Sekolah</b></label>
                                                            <input type="file" class="form-control" name="logo_school">
                                                        </div>
                                                    </div>
                                                    <!-- Sambutan End -->
                                                    <div class="form-group mt_30">
                                                        <button type="submit" class="btn btn-primary">Perbarui</button>
                                                    </div>
                                                </form>
                                            </div>
                                            
                                            {{-- <div class="pt_0 tab-pane fade" id="v-4" role="tabpanel" aria-labelledby="v-4-tab">
                                                
                                                <form method="">
                                                @csrf
                                                    @foreach($links as $link)
                                                        <div class="row">
                                                            <div class="form-group col-md-5 mb-3">
                                                                <label>Nama Tautan</label>
                                                                <input type="text" id="nama_link" class="form-control" name="nama_link" value="{{ $link->nama_link }}">
                                                            </div>
                                                            <div class="form-group col-md-5 mb-3">
                                                                <label>Alamat Tautan</label>
                                                                <input type="text" class="form-control" name="alamat_link" value="{{ $link->alamat_link }}">
                                                            </div> 
                                                            <div class="form-group col-md-2 mt-5">
                                                                <a href="#" data-id="{{ $link->id }}" class="btn btn-info btn-sm update-btn">
                                                                    <span class="fa fa-pencil-alt"></span>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </form>
                                            </div> --}}

                                        </div>
                                        
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection