<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">Portal Berita Sekolah</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="#">PBS</a>
        </div>

        <ul class="sidebar-menu">

            <li class="{{ Request::is('admin/home') ? 'active' : ''}}"><a class="nav-link" href="{{ route('admin.home') }}"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>

            <li class="{{ Request::is('admin/post/*') ? 'active' : ''}}"><a class="nav-link" href="{{ route('post.show') }}"><i class="fas fa-newspaper"></i> <span>Berita</span></a></li>

            <li class="nav-item dropdown {{ Request::is('admin/category/*') || Request::is() ? 'active' : ''}}">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-window-restore"></i><span>Widget</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/category/*') ? 'active' : ''}}"><a class="nav-link" href="{{ route('category.show') }}"><i class="fas fa-tag"></i>Kategori</a></li>
                </ul>
            </li>

            

            <li class="nav-item dropdown {{ Request::is('admin/visi-misi') || Request::is('admin/struktur') || Request::is('admin/akreditasi') || Request::is('admin/sejarah')  ? 'active' : ''}}">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-school"></i><span>Profile Sekolah</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/visi-misi') ? 'active' : ''}}"><a class="nav-link" href="{{ route('profile.visi') }}"><i class="fas fa-tag"></i>Visi Misi</a></li>
                    <li class="{{ Request::is('admin/struktur') ? 'active' : ''}}"><a class="nav-link" href="{{ route('profile.struktur') }}"><i class="fas fa-tag"></i>Struktur</a></li>
                    <li class="{{ Request::is('admin/akreditasi') ? 'active' : ''}}"><a class="nav-link" href="{{ route('profile.akreditasi') }}"><i class="fas fa-tag"></i>Akreditasi</a></li>
                    <li class="{{ Request::is('admin/sejarah') ? 'active' : ''}}"><a class="nav-link" href="{{ route('profile.sejarah')}}"><i class="fas fa-tag"></i>Sejarah</a></li>
                </ul>
            </li>

            <li class="{{ Request::is('admin/author/show') ? 'active' : ''}}"><a class="nav-link" href="{{ route('author.show') }}"><i class="fas fa-users"></i> <span>Daftar Penulis</span></a></li>
            

            <li class="{{ Request::is('admin/setting') ? 'active' : ''}}"><a class="nav-link" href="{{ route('setting.index') }}"><i class="fas fa-cogs"></i> <span>Pengaturan Beranda</span></a></li>

        </ul>
    </aside>
</div>