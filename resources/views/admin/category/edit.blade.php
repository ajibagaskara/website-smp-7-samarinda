@extends('admin.layout.master')

@section('content')
    <div class="main-content">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('category.update', $category->id) }}" method="post">
                                @csrf
                                <div class="form-group mb-3">
                                    <label>Nama Kategori</label>
                                    <input type="text" class="form-control" name="category_name" value="{{ $category->category_name}}">
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Perbarui</button>
                                    <a href="{{ route('category.show') }}" class="btn btn-info">Kembali</a>
                                </div>
                                @if($post->isEmpty())
                                    <a href="#" data-id="{{$category->id}}" class="btn btn-danger delete-btn">
                                        Hapus Kategori
                                    </a>
                                @else
                                    
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('.delete-btn').click( function(){
            var id_data = $(this).attr('data-id');
            swal({
                title: "Konfirmasi Penghapusan",
                text: "Apakah anda ingin menghapus data?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            // admin/category/delete/{id}
            .then((willDelete) => {
                if (willDelete) {
                    window.location = "/admin/category/delete/"+id_data
                } else {
                    swal("Proses Penghapusan Dibatalkan");
                }
            });
        });
    </script>
@endsection