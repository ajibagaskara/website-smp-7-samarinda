@extends('admin.layout.master')

@section('content')
    <div class="main-content">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('struktur.update') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group mb-3">
                                    <div class="row mt-3">
                                        <div class="col-md-6">
                                            <label><b>Bagan Organisasi</b></label>
                                            <img src="{{asset('uploads/'.$struktur->struktur_img) }}" alt="Gambar Berita" style="width:300px; display:block;">
                                        </div>
                                        <div class="col-md-6">
                                            <label><b>Ganti Bagan Organisasi</b></label>
                                            <input type="file" class="form-control" name="struktur_img">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label>Struktur Organisasi</label>
                                    <textarea name="struktur_desk" class="form-control" id="summernote">{{$struktur->struktur_desk}}</textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Perbarui</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection