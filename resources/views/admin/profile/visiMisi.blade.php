@extends('admin.layout.master')

@section('content')
    <div class="main-content">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('visi.update')}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group mb-3">
                                    <label>Visi</label>
                                    <textarea name="visi" class="form-control" id="summernote">
                                        {{$visi->visi}}
                                    </textarea>
                                </div>
                                <div class="form-group mb-3">
                                    <label>Misi</label>
                                    <textarea name="misi" class="form-control" id="summernote1">
                                        {{$visi->misi}}
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Perbarui</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection