@extends('admin.layout.master')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Daftar Berita</h1>
                <div class="ml-auto">
                    <a href="{{ route('post.create') }}" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah Berita</a>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="example1">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Gambar Berita</th>
                                                <th>Judul Berita</th>
                                                <th>Kategori</th>
                                                <th>Status</th>
                                                <th>Penulis</th>
                                                <th>Pilihan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($posts as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td width:>
                                                    <img src="{{ asset('uploads/'.$row->post_photo) }}" alt="Thumbnail Berita" style="width: 100px;">
                                                </td>
                                                <td>{{ $row->post_title}}</td>
                                                <td>{{ $row->nCategory->category_name}}</td>
                                               
                                                <td>
                                                    @if($row->status_post == "Penting")
                                                    <span class="badge badge-success">{{ $row->status_post}}</span>
                                                    @elseif($row->status_post == "Umum")
                                                    <span class="badge badge-warning">{{ $row->status_post}}</span>
                                                    @else
                                                    <span class="badge badge-danger">{{ $row->status_post}}</span>
                                                    @endif
                                                </td>
                                                
                                                <td>

                                                    @if($row->author_id != 0)

                                                        {{-- tangkap nama penulis dalam variable --}}
                                                        {{ \App\Models\Author::where('id', $row->author_id)->first()->name }}

                                                    @elseif($row->admin_id != 0)
                                                    {!! Auth::guard('admin')->user()->name !!}
                                                    @endif
                                                </td>
                                                <td class="pt_10 pb_10">
                                                    <a href="{{ route('post.edit', $row->id)}}" class="btn btn-info btn-sm" title="Edit Data">
                                                        <i class="fas fa-pencil-alt"></i>
                                                    </a>
                                                    <a href="#" data-id="{{$row->id}}" class="btn btn-danger btn-sm delete-btn" title="Hapus Data">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </div>
@endsection
@section('script')
    <script>
        $('.delete-btn').click( function(){
            var id_data = $(this).attr('data-id');
            swal({
                title: "Konfirmasi Penghapusan",
                text: "Apakah anda ingin menghapus data?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    window.location = "/admin/post/delete/"+id_data
                } else {
                    swal("Proses Penghapusan Dibatalkan");
                }
            });
        });
    </script>
@endsection