@extends('admin.layout.master')

@section('content')
    <div class="main-content">
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('post.update', $post->id) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group mb-3">
                                    <label>Judul Berita</label>
                                    <input type="text" class="form-control" name="post_title" value="{{ $post->post_title }}">
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label><b>Kategori Berita</b></label>
                                        <select name="category_id" class="form-control">
                                            @foreach($categories as $item)
                                                <option value="{{ $item->id }}"
                                                @if($item->id == $post->category_id) selected @endif>{{ $item->category_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label><b>Status Berita</b></label>
                                        <select name="status_post" class="form-control"> 
                                            <option value="Penting" @if($post->status_post == 'Penting') selected @endif>Penting</option>
                                            <option value="Umum" @if($post->status_post == 'Umum') selected @endif>Umum</option>
                                            <option value="Draft" @if($post->status_post == 'Draft') selected @endif>Draft</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-6">
                                        <label><b>Gambar Berita</b></label>
                                        <img src="{{asset('uploads/'.$post->post_photo)}}" alt="Gambar Berita" style="width:300px; display:block;">
                                    </div>
                                    <div class="col-md-6">
                                        <label><b>Ganti Gambar Berita</b></label>
                                        <input type="file" class="form-control" name="post_photo">
                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <label>Detail Berita</label>
                                    <textarea name="post_detail" class="form-control" id="summernote">{{$post->post_detail}}</textarea>
                                </div>
                                <div class="form-group mb-3">
                                    <label><b>Slider Berita</b></label>
                                    <select name="show_slider" class="form-control">
                                        <option value="Aktif" @if($post->show_slider == 'Aktif') selected @endif</option>Aktif</option>
                                        <option value="Tidak Aktif" @if($post->show_slider == 'Tidak Aktif') selected @endif</option>Tidak Aktif</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Perbarui</button>
                                    <a href="{{ route('post.show') }}" class="btn btn-info">Kembali</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection