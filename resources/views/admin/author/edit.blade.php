@extends('admin.layout.master')

@section('content')
    <div class="main-content">
        <div class="section-body">
            <form action="{{ route('author.update',$author->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                @csrf
                                <div class="form-group mb-3">
                                    <label>Nama Penulis</label>
                                    <input type="text" class="form-control" name="name" value="{{ $author->name }}">
                                </div>
                                <div class="form-group mb-3">
                                    <label>Email Penulis</label>
                                    <input type="email" class="form-control" name="email" value="{{ $author->email }}">
                                </div>
                                <div class="form-group mb-3">
                                    <label class="form-label">Kata Sandi</label>
                                    <input type="password" class="form-control" name="password">
                                </div>
                                <div class="form-group mb-3">
                                    <label class="form-label">Tulis Ulang Sandi</label>
                                    <input type="password" class="form-control" name="retype_password">
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Perbarui</button>
                                    <a href="{{ route('author.show') }}" class="btn btn-info">Kembali</a>
                                    @if($post->isEmpty())
                                        <a href="#" data-id="{{$author->id}}" class="btn btn-danger delete-btn">
                                            Hapus Penulis
                                        </a>
                                    @else
                                        <span class="btn btn-success">Penulis Ini Memiliki Berita</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group mb-3">
                                    <label>Ganti Gambar</label>
                                    <input type="file" class="form-control" name="photo">
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group mb-3">
                                    <label>Gambar Penulis</label>
                                    <img src="{{ asset('uploads/'.$author->photo)}}" style="width: 300px;" >
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </form>  
        </div>
    </div>
@endsection
{{-- SCRIPT LOGIC DELETE DATA --}}
@section('script')
    <script>
        $('.delete-btn').click( function(){
            var id_data = $(this).attr('data-id');
            swal({
                title: "Konfirmasi Penghapusan",
                text: "Apakah anda ingin menghapus data?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    window.location = "/admin/author/delete/"+id_data
                } else {
                    swal("Proses Penghapusan Dibatalkan");
                }
            });
        });
    </script>
@endsection