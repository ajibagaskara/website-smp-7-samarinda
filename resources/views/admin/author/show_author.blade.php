@extends('admin.layout.master')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Daftar Penulis Berita</h1>
                <div class="ml-auto">
                    <a href="{{ route('author.create') }}" class="btn btn-primary"><i class="fas fa-plus"></i> Tambah Penulis</a>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="example1">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Pilihan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($authors as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $row->name }}</td>
                                                <td class="pt_10 pb_10">
                                                    <a href="{{ route('author.edit', $row->id)}}" class="btn btn-info btn-sm" title="Edit Data">
                                                        <i class="fas fa-pencil-alt"></i>
                                                    </a>
                                                    {{-- <a href="#" data-id="{{$row->id}}" class="btn btn-danger btn-sm delete-btn" title="Hapus Data">
                                                        <i class="fas fa-trash"></i>
                                                    </a> --}}
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
    </div>
@endsection
{{-- @section('script')
    <script>
        $('.delete-btn').click( function(){
            var id_data = $(this).attr('data-id');
            swal({
                title: "Konfirmasi Penghapusan",
                text: "Apakah anda ingin menghapus data?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    window.location = "/admin/author/delete/"+id_data
                } else {
                    swal("Proses Penghapusan Dibatalkan");
                }
            });
        });
    </script>
@endsection --}}