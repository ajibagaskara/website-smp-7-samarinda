@extends('admin.author.layout.master')

@section('content')
    <div class="main-content">
        <div class="section-body">
            
            <form action="{{ route('author.post.store') }}" method="post" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-body">
                                @csrf
                                <div class="form-group mb-3">
                                    <label>Judul Berita</label>
                                    <input type="text" class="form-control" name="post_title">
                                </div>
                                <div class="row mb-3">
                                    {{-- <div class="col-md-6">
                                        <label><b>Status Berita</b></label>
                                        <select name="status_post" class="form-control">
                                            <option value="Penting">Penting</option>
                                            <option value="Umum">Umum</option>
                                        </select>
                                    </div> --}}
                                    <div class="col-md-12">
                                        <label><b>Kategori Berita</b></label>
                                        <select name="category_id" class="form-control">
                                            <option disabled selected>PILIH KATEGORI</option>
                                            @foreach($categories as $item)
                                                <option value="{{ $item->id }}">{{ $item->category_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                    <div class="col-md-6">
                                        <label><b>Bagikan Berita</b></label>
                                        <select name="is_share" class="form-control">
                                            <option value="1">Aktif</option>
                                            <option value="0">Tidak Aktif</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label><b>Komentar Berita</b></label>
                                        <select name="is_comment" class="form-control">
                                            <option value="1">Aktif</option>
                                            <option value="0">Tidak Aktif</option>
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="form-group mb-3">
                                    <label>Detail Berita</label>
                                    <textarea name="post_detail" class="form-control" id="summernote"></textarea>
                                </div>
                                {{-- <div class="form-group mb-3">
                                    <label><b>Slider Berita</b></label>
                                    <select name="show_slider" class="form-control">
                                        <option value="Aktif">Aktif</option>
                                        <option value="Tidak Aktif">Tidak Aktif</option>
                                    </select>
                                </div> --}}
                            </div>
                            <div class="card-footer">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Simpan</button>
                                    <a href="#" class="btn btn-info">Kembali</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group mb-3">
                                    <label>Gambar Berita</label>
                                    <input type="file" class="form-control" name="post_photo" onchange="loadFile(event)">

                                    <img id="output" style="width: 300px;">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </form>  
        </div>
    </div>
@endsection
@section('script')
    <script>
        var loadFile = function(event){
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
        }
    </script>
@endsection