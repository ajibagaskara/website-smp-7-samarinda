<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="#">Portal Berita Sekolah</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="#">PBS</a>
        </div>

        <ul class="sidebar-menu">

            <li class="{{ Request::is('author/home') ? 'active' : ''}}"><a class="nav-link" href="{{ route('author.home') }}"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>

            <li class="{{ Request::is('author/post/show') ? 'active' : ''}}"><a class="nav-link" href="{{ route('author.post.show')}}"><i class="fas fa-book"></i> <span>Berita</span></a></li>

        </ul>
    </aside>
</div>