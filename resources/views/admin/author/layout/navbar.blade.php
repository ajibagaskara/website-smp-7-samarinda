<div class="navbar-bg"></div>
<nav class="navbar navbar-expand-lg main-navbar">
        <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
        </ul>
    <ul class="navbar-nav navbar-right">
        <li class="nav-link">
            <a href="{{ route('home')}}" target="_blank" class="btn btn-warning">Lihat Website</a>
        </li>
        <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="image" src="{{ asset('uploads/'.Auth::guard('author')->user()->photo) }}" 
            class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">{{ Auth::guard('author')->user()->name }}</div></a>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="{{ route('author.logout') }}" class="dropdown-item has-icon text-danger">
                    <i class="fas fa-sign-out-alt"></i> Keluar
                </a>
            </div>
        </li>
    </ul>
</nav>