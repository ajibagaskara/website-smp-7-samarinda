@extends('admin.layout.master')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Daftar Kategori</h1>
                <div class="ml-auto">
                    <a href="#" class="btn btn-primary"><i class="fas fa-plus"></i></a>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="example1">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Kategori</th>
                                                <th>Pilihan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {{-- @foreach($categories as $row)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $row->category_name}}</td>
                                                <td class="pt_10 pb_10">
                                                    <a href="{{ route('category.edit', $row->id) }}" class="btn btn-info btn-sm" title="Edit Data">
                                                        <i class="fas fa-pencil-alt"></i>
                                                    </a>    
                                                </td>
                                            </tr>
                                            @endforeach --}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection