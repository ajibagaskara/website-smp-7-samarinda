<?php

namespace App\Http\Controllers\Admin;

use App\Models\Akreditasi;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminAkreditasiController extends Controller
{
    //show akreditasi page
    public function index()
    {
        $akreditasi = Akreditasi::where('id',1)->first();
        return view('admin.profile.akreditasi', compact('akreditasi'));
    }

    public function updateAkreditasi(Request $request)
    {
        //validasi request dari form
        $request->validate([
            'detail_akreditasi' => 'required'
        ],
    [
        'detail_akreditasi.required' => 'Data Tidak Boleh Kosong'
    ]);

        $akreditasi = Akreditasi::where('id',1)->first();
        $akreditasi->detail_akreditasi = $request->detail_akreditasi;
        $akreditasi->update();

        return redirect()->route('profile.akreditasi')->with('success', 'Data Berhasil Diperbarui');
    }
}
