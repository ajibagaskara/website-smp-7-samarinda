<?php

namespace App\Http\Controllers\Admin;


// use DB;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthorPostController extends Controller
{
    //menampilkans semua berita
    public function show_post()
    {
        //get all data from table category and order it asc
        $posts = Post::with('nCategory')->where('author_id', Auth::guard('author')->user()->id)->get();
        return view('admin.author.post_show', compact('posts'));
    }

    public function create()
    {
        $categories = Category::get();
        // dd($categories);
        // foreach($categories as $item){
        //     echo $item->category_name. '<br>';
        // }
        return view('admin.author.post_create', compact('categories'));
    }

    public function store(Request $request)
    {
        //validasi request untuk judul post & detail post
        $request->validate([
            'post_title' => 'required',
            'post_detail' => 'required',
            'category_id' => 'required',
            'post_photo' => 'required|mimes:jpg,jpeg,png,gif'
        ],
        [
            'post_title.required' => 'Judul Berita Tidak Boleh Kosong',
            'post_detail.required' => 'Detil Berita Tidak Boleh Kosong',
            'category_id.required' => 'Kategori Berita Belum Dipilih',
            'post_photo.required' => 'Gambar Berita Belum Dipilih',
            'post_photo.mimes' => 'Format Gambar Harus jpg/jpeg/png'
        ]);

        //Kelola request gambar
        $now = time();
        $ext = $request->file('post_photo')->extension();
        $final_name = 'post_photo_'.$now.'.'.$ext;
        $request->file('post_photo')->move(public_path('uploads'),$final_name);

        //instansi class dari Model Post
        $post = new Post();

        //terima nilai dari request
        $post->category_id = $request->category_id;
        $post->post_title = $request->post_title;
        $post->post_detail = trim($request->post_detail);
        $post->post_photo = $final_name;
        $post->status_post = "Draft";
        $post->show_slider = "Tidak Aktif";
        $post->visitors = 1;
        $post->author_id = Auth::guard('author')->user()->id;
        $post->admin_id = 0;

        //simpan
        $post->save();

        return redirect()->route('author.post.show')->with('success', 'Data Berhasil Disimpan');
    }

    public function edit($id)
    {
        //check id for posts that belongs to this author
        $cek_post = Post::where('id', $id)->where('author_id', Auth::guard('author')->user()->id)->count();

        //if the user try to access the id that is not belong to its author then redirect it to author.post.show
        if(!$cek_post){
            return redirect()->route('author.post.show');
        }

        $categories = Category::get();
        $post = Post::where('id', $id)->first();
        // dd($exist_tags);
        return view('admin.author.post_edit', compact('post', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'post_title' => 'required',
            'post_detail' => 'required'
        ],
    [
        'post_title.required' => 'Judul Berita Tidak Boleh Kosong',
        'post_detail.required' => 'Detil Berita Tidak Boleh Kosong'
    ]);

        $post = Post::where('id', $id)->first();

        if($request->hasFile('post_photo')){
            $request->validate([
                'post_photo' => 'image|mimes:jpg,jpeg,png'
            ],
        [
            'post_photo.mimes' => 'Format Gambar Harus jpg/jpeg/png'
        ]);

            unlink(public_path('uploads/'.$post->post_photo));

            $now = time();
            $ext = $request->file('post_photo')->extension();
            $final_name = 'post_photo_'.$now.'.'.$ext;
            $request->file('post_photo')->move(public_path('uploads/'),$final_name);
            $post->post_photo = $final_name;
        }

        $post->category_id = $request->category_id;
        $post->post_title = $request->post_title;
        $post->post_detail = trim($request->post_detail);
        $post->status_post = "Draft";
        $post->show_slider = "Tidak Aktif";
        $post->update();

        return redirect()->route('author.post.show')->with('success', 'Data Berhasil Diperbarui');

    }

    public function delete($id)
    {
        //Pilih data berdasarkan id
        $post = Post::where('id', $id)->first();

        //hapus gambar dari folder public/uploads
        unlink(public_path('uploads/'.$post->post_photo));

        //Delete data
        $post->delete();

        //Arahkan kembali ke halaman category.show dengan pesan
        return redirect()->route('author.post.show')->with('success', 'Data Berhasil Dihapus');
    }
}
