<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminProfileController extends Controller
{
    
    public function index()
    {
        //redirect to edit profile page
        return view('admin.profile');
    }

    public function profileSubmit(Request $request){

        $admin_data = Admin::where('email', Auth::guard('admin')->user()->email)->first();
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'no_phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10'
        ],
        [
            'name.required' => 'Nama Admin Wajib Di-isi',
            'email.required' => 'Email Admin Wajib Di-isi',
            'no_phone.required' => 'Nomor Telpon Admin Wajib Di-isi',
            'no_phone.regex' => 'Format Nomor Telpon Salah',
            'no_phone.min' => 'Minimal Digit Nomor Telpon 10 Angka'
        ]);

        if($request->password != ''){
            $request->validate([
                'password' => 'required',
                'retype_password' => 'required|same:password'
            ],
        [
            'password.required' => 'Password Tidak Boleh kosong',
            'retype_password.required' => 'Password Tidak Boleh kosong',
            'retype_password.required' => 'password Tidak Sama'
        ]);
            $admin_data->password = Hash::make($request->password);
        }

        if($request->hasFile('photo')){
            $request->validate([
                'photo' => 'image|mimes:jpg,jpeg,png'
            ],
        [
            'photo.mines' => 'File harus Bertipe jpg,jpeg,png',
            'photo.image' => 'File harus Gambar'
        ]);
            
            //remove the file image from public folder
            unlink(public_path('uploads/'.$admin_data->photo));

            //get the extension of image from request
            $ext = $request->file('photo')->extension();

            //name the uploaded image
            $img_name = 'admin'.'.'.$ext;

            //moved image to public folder with its name
            $request->file('photo')->move(public_path('uploads/'), $img_name);

            //update the image in database
            $admin_data->photo = $img_name;
        }

        $admin_data->name = $request->name;
        $admin_data->email = $request->email;
        $admin_data->no_phone = $request->no_phone;
        $admin_data->update();

        return redirect()->back()->with('success', 'Profile Berhasil Diperbarui');
    }
}
