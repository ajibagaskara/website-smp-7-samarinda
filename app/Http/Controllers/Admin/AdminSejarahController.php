<?php

namespace App\Http\Controllers\Admin;

use App\Models\Sejarah;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminSejarahController extends Controller
{
    //show sejarah page
    public function index()
    {
        $sejarah = Sejarah::where('id',1)->first();
        return view('admin.profile.sejarah', compact('sejarah'));
    }

    public function updateSejarah(Request $request)
    {
        //validasi request dari form
        $request->validate([
            'sejarah' => 'required',
        ],
    [
        'sejarah.required' => 'Data Tidak Boleh Kosong'
    ]);

        $sejarah = Sejarah::where('id',1)->first();
        $sejarah->sejarah_sekolah = $request->sejarah;
        $sejarah->update();

        return redirect()->route('profile.sejarah')->with('success', 'Data Berhasil Diperbarui');
    }
}
