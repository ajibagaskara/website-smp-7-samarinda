<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use App\Models\Author;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminHomeController extends Controller
{
    //Go to home.blade.php 
    public function index(){
        //hitung jumlah semua kategori,post,dan author
        $total_categories = Category::count();
        $total_posts = Post::count();
        $total_authors = Author::count();
        return view('admin.home', compact('total_categories','total_posts','total_authors'));
    }
}
