<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
// use Hash;

class AdminLoginController extends Controller
{
    //ARAHKAN KE HALAMAN LOGIN
    public function login()
    {
        // $pass = Hash::make('backdoor1234');
        // $pass = Hash::make('admin-01');
        // $pass = Hash::make('admin-02');
        
        // dd($pass);
        return view('admin.auth.login');
    }

    //ARAHKAN KE HALAMAN FORGET PASSWORD
    public function forget()
    {
        return view('admin.auth.forget-pass');
    }

    //SUBMIT LOGIN
    public function loginSubmit(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ],
    [
        'email.required' => 'Email Tidak Boleh Kosong',
        'email.email' => 'Data yang dimasukkan Harus Pertipe Email',
        'password.required' => 'Passworld Harus DI Isi'
    ]);

        $credential = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if(Auth::guard('admin')->attempt($credential)){
            return redirect()->route('admin.home');
        }else{
            return redirect()->route('admin.login')->with('error', 'Email or Password is Wrong');
        }
    }

    //LOGOUT
    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }

    //RESET PASSWORD
    public function forgetPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email'
        ],
    [
        'email.required' => 'Email Tidak Boleh Kosong',
        'email.email' => 'Data yang dimasukkan Harus Pertipe Email'
    ]);

        $result = Admin::where('email', $request->email)->first();

        if(!$result){
            return redirect()->back()->with('error', 'Email Address Not Found');
        }
    }
    
}
