<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminCategoryController extends Controller
{
    public function create()
    {
        
        return view('admin.category.create');
    }

    public function show()
    {
        //get all data from table category and order it asc
        $categories = Category::get();
        // $post_detail = Post::with('nCategory')->where('id',$id)->first();
        //check if this category has posts
        // $get_post = Category::has('mposts')->count();
        // $post = Post::with('nCategory')->get();
        // dd($get_post);
        return view('admin.category.index', compact('categories'));
    }

    public function store(Request $request)
    {
        //Validasi request yang masuk dari form tambah kategori
        $request->validate([
            'category_name' => 'required'  
        ],
    [
        'ctegory_name.requird' => 'Nama Kategori tidak Boleh ksong'
    ]);

        //Instansi objek dari class Category
        $category = new Category();

        //Terima inputan $request
        $category->category_name = $request->category_name;

        //Simpan data
        $category->save();

        //Arahkan kembali ke halaman category.show dengan pesan
        return redirect()->route('category.show')->with('success', 'Data Berhasil Disimpan');
    }

    public function edit($id)
    {
        $category = Category::where('id', $id)->first();
        $post = Post::where('category_id',$id)->get();
        return view('admin.category.edit', compact('category','post'));
    }

    public function update(Request $request, $id)
    {
        //Validasi request yang masuk dari form tambah kategori
        $request->validate([
            'category_name' => 'required'
        ],
        [
            'ctegory_name.required' => 'Nama Kategori tidak Boleh ksong'
        ]);

        //Pilih data berdasarkan id
        $category = Category::where('id', $id)->first();

        //Terima semua inputan dari $request
        $category->category_name = $request->category_name;
        //Update data
        $category->update();

        //Arahkan kembali ke halaman category.show dengan pesan
        return redirect()->route('category.show')->with('success', 'Data Berhasil Diperbarui');
    }

    public function delete($id)
    {
        //Pilih data berdasarkan id
        $category = Category::where('id', $id)->first();

        //Delete data
        $category->delete();

        //Arahkan kembali ke halaman category.show dengan pesan
        return redirect()->route('category.show')->with('success', 'Data Berhasil Dihapus');
    }
}
