<?php

namespace App\Http\Controllers\Admin;

use App\Models\Logo;
use App\Models\Message;
use App\Models\Setting;
use App\Models\Links;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminSettingController extends Controller
{
    public function index()
    {
        $logo = Logo::where('id',1)->first();
        $setting = Setting::where('id',1)->first();
        $message = Message::where('id',1)->first();
        $links = Links::get();
        // foreach($links as $link){
        //     $test = $link->id;
        // }
        // dd($test);
        return view('admin.setting.index', compact('setting','message','logo','links'));
    }

    public function update(Request $request)
    {
        //validasi request dari form
        $request->validate([
            'news_ticker_total' => 'required'
        ],
    [
        'news_ticker_total.required' => 'Data Harus Di Isi'
    ]);

        $setting = Setting::where('id',1)->first();
        $setting->news_ticker_total = $request->news_ticker_total;
        $setting->news_ticker_status = $request->news_ticker_status;
        $setting->update();

        return redirect()->route('setting.index')->with('success', 'Data Berhasil Diperbarui');
    }


    //method to handle sambutan kepala sekolah
    public function updateMessage(Request $request)
    {
        //validasi request dari form
        $request->validate([
            'name' => 'required',
        ],
    [
        'name.required' => 'Nama Kepalah Skolah Harus Di Isi'
    ]);

        $message = Message::where('id',1)->first();

        if($request->hasFile('msg_photo')){
            $request->validate([
                'msg_photo' => 'image|mimes:jpg,jpeg,png'
            ],
        [
            'msg_photo.mines' => 'File harus Bertipe jpg,jpeg,png',
            'msg_photo.image' => 'File harus Gambar'
        ]);

            unlink(public_path('uploads/'.$message->msg_photo));

            $now = time();
            $ext = $request->file('msg_photo')->extension();
            $final_name = 'msg_photo_'.$now.'.'.$ext;
            $request->file('msg_photo')->move(public_path('uploads/'),$final_name);
            $message->msg_photo = $final_name;
        }

        $message->name = $request->name;
        $message->msg = $request->msg;
        $message->position = $request->position;
        $message->update();

        return redirect()->route('setting.index')->with('success', 'Data Berhasil Diperbarui');
    }

    public function updateLogo(Request $request)
    {
        //fetch data based on id, take the first data on the row
        $logo = Logo::where('id',1)->first();

        //if there are request for logo_school & ic_website
        if($request->hasFile('logo_school')){

            // validate request to change logo_school & ic_website
            $request->validate([
                'logo_school' => 'image|mimes:jpg,jpeg,png',
            ],
        [
            'logo_school.mines' => 'File harus Bertipe jpg,jpeg,png',
            'logo_school.image' => 'File harus Gambar'
        ]);

            //delete the current image
            unlink(public_path('uploads/'.$logo->logo));

            $now = time();
            $ext = $request->file('logo_school')->extension();
            $final_name = 'logo_school_'.$now.'.'.$ext;
            $request->file('logo_school')->move(public_path('uploads/'),$final_name);
            $logo->logo = $final_name;
        }
        $logo->update();
        return redirect()->route('setting.index')->with('success', 'Data Berhasil Diperbarui');
    }

    public function updateLink(Request $request, $id)
    {
        $links = Links::where('id',$id)->first();

        $links->nama_link = $request->nama_link;
        $links->alamat_link = $request->alamat_link;

        dd($links);
        // $links->update();


        //Arahkan kembali ke halaman setting.index dengan pesan
        // return redirect()->route('setting.index')->with('success', 'Data Berhasil Diperbarui');
    }
}
