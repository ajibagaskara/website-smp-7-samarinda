<?php

namespace App\Http\Controllers\Admin;

use App\Models\Vision;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminVisiMisiController extends Controller
{
    //show visi misi page
    public function index()
    {
        $visi = Vision::where('id',1)->first();
        return view('admin.profile.visiMisi', compact('visi'));
    }

    public function updateVisiMisi(Request $request)
    {
        //validasi request dari form
        $request->validate([
            'visi' => 'required',
            'misi' => 'required'
        ],
    [
        'visi.required' => 'Visi Harus di isi',
        'misi.required' => 'Misi Harus di isi'
    ]);

        $visi = Vision::where('id',1)->first();
        $visi->visi = $request->visi;
        $visi->misi = $request->misi;
        $visi->update();

        return redirect()->route('profile.visi')->with('success', 'Data Berhasil Diperbarui');
    }
}
