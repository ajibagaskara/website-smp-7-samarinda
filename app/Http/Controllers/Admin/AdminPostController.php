<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;;
use App\Http\Controllers\Controller;

class AdminPostController extends Controller
{
    public function show()
    {
        //get all data from table category and order it asc
        $posts = Post::with('nCategory')->get();
        // dd($posts);
        return view('admin.post.index', compact('posts'));
    }

    public function create()
    {
        $categories = Category::get();
        // dd($categories);
        // foreach($categories as $item){
        //     echo $item->category_name. '<br>';
        // }
        return view('admin.post.create', compact('categories'));
    }

    public function store(Request $request)
    {
        //validasi request untuk judul post & detail post
        $request->validate([
            'post_title' => 'required',
            'post_detail' => 'required',
            'category_id' => 'required',
            'post_photo' => 'required|mimes:jpg,jpeg,png'
        ],
        [
            'post_title.required' => 'Judul Berita Tidak Boleh Kosong',
            'post_detail.required' => 'Detil Berita Tidak Boleh Kosong',
            'category_id.required' => 'Kategori Berita Belum Dipilih',
            'post_photo.required' => 'Gambar Berita Belum Dipilih',
            'post_photo.mimes' => 'Format Gambar Harus jpg/jpeg/png'
        ]);

        //Kelola request gambar
        
        $now = time();
        $ext = $request->file('post_photo')->extension();
        $final_name = 'post_photo_'.$now.'.'.$ext;
        $request->file('post_photo')->move(public_path('uploads'),$final_name);

        //instansi class dari Model Post
        $post = new Post();

        //terima nilai dari request
        $post->category_id = $request->category_id;
        $post->post_title = $request->post_title;
        $post->post_detail = trim($request->post_detail);
        $post->post_photo = $final_name;
        $post->status_post = $request->status_post;
        $post->show_slider = $request->show_slider;
        $post->visitors = 1;
        $post->author_id = 0;
        $post->admin_id = Auth::guard('admin')->user()->id;

        //simpan
        $post->save();

        return redirect()->route('post.show')->with('success', 'Data Berhasil Disimpan');
    }

    public function edit($id)
    {
        $categories = Category::get();
        $post = Post::where('id', $id)->first();
        // $author = Author::where('id',$id)->first();
        // dd($post);
        return view('admin.post.edit', compact('post', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'post_title' => 'required',
            'post_detail' => 'required'
        ],
    [
        'post_title.required' => 'Judul Berita Tidak Boleh Kosong',
        'post_detail.required' => 'Detil Berita Tidak Boleh Kosong'
    ]);

        $post = Post::where('id', $id)->first();

        if($request->hasFile('post_photo')){
            $request->validate([
                'post_photo' => 'image|mimes:jpg,jpeg,png'
            ],
            [
                'post_photo.required' => 'Gambar Berita Belum Dipilih',
                'post_photo.mimes' => 'Format Gambar Harus jpg/jpeg/png'
            ]
        );

            unlink(public_path('uploads/'.$post->post_photo));

            $now = time();
            $ext = $request->file('post_photo')->extension();
            $final_name = 'post_photo_'.$now.'.'.$ext;
            $request->file('post_photo')->move(public_path('uploads/'),$final_name);
            $post->post_photo = $final_name;
        }

        $post->category_id = $request->category_id;
        $post->post_title = $request->post_title;
        $post->post_detail = trim($request->post_detail);
        $post->status_post = $request->status_post;
        $post->show_slider = $request->show_slider;
        $post->update();

        return redirect()->route('post.show')->with('success', 'Data Berhasil Diperbarui');
    }

    public function delete($id)
    {
        //Pilih data berdasarkan id
        $post = Post::where('id', $id)->first();

        //hapus gambar dari folder public/uploads
        unlink(public_path('uploads/'.$post->post_photo));

        //Delete data
        $post->delete();

        //Arahkan kembali ke halaman category.show dengan pesan
        return redirect()->route('post.show')->with('success', 'Data Berhasil Dihapus');
    }

}
