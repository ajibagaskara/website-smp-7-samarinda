<?php

namespace App\Http\Controllers\Admin;

use App\Models\Struk;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminStrukturController extends Controller
{
    public function index()
    {
        $struktur = Struk::where('id',1)->first();
        return view('admin.profile.struktur', compact('struktur'));
    }

    public function updateStruktur(Request $request)
    {
        //validasi request dari form
        $request->validate([
            'struktur_desk' => 'required'
        ],
    [
        'struktur_desk.required' => 'Data Tidak Boleh Kosong'
    ]);

        $struktur = Struk::where('id',1)->first();

        if($request->hasFile('struktur_img')){
            $request->validate([
                'struktur_img' => 'image|mimes:jpg,jpeg,png'
            ],
        [
            'struktur_img.mines' => 'File harus Bertipe jpg,jpeg,png',
            'struktur_img.image' => 'File harus Gambar'
        ]);

            unlink(public_path('uploads/'.$struktur->struktur_img));

            $now = time();
            $ext = $request->file('struktur_img')->extension();
            $final_name = 'struktur_photo_'.$now.'.'.$ext;
            $request->file('struktur_img')->move(public_path('uploads/'),$final_name);
            $struktur->struktur_img = $final_name;
        }

        $struktur->struktur_desk = $request->struktur_desk;
        $struktur->update();

        return redirect()->route('profile.struktur')->with('success', 'Data Berhasil Diperbarui');
    }
}
