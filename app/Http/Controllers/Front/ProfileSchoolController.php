<?php

namespace App\Http\Controllers\Front;

use App\Models\Logo;
use App\Models\Admin;
use App\Models\Struk;
use App\Models\Vision;
use App\Models\Message;
use App\Models\Sejarah;
use App\Models\Akreditasi;
use App\Http\Controllers\Controller;

class ProfileSchoolController extends Controller
{
    public function visiMisi()
    {
        $admin = Admin::where('id',1)->get();
        foreach($admin as $item)
        {
            $admin_email = $item->email;
            $admin_phone = $item->no_phone;
        }
        $logo = Logo::get();
        foreach($logo as $item ) {
            $logo_school = $item->logo;
        }
        $visi = Vision::get();
        foreach($visi as $item)
        {
            $visi = $item->visi;
            $misi = $item->misi;
        }
        return view('front.profile.visi-misi', compact('admin_email','admin_phone','logo_school','visi','misi'));
    }

    public function struktur()
    {
        $admin = Admin::where('id',1)->get();
        foreach($admin as $item)
        {
            $admin_email = $item->email;
            $admin_phone = $item->no_phone;
        }
        $logo = Logo::get();
        foreach($logo as $item ) {
            $logo_school = $item->logo;
        }
        $struk = Struk::get();
        foreach($struk as $item){
            $img = $item->struktur_img;
            $desk = $item->struktur_desk;
        }
        return view('front.profile.struktur', compact('admin_email','admin_phone','logo_school','img','desk'));
    }

    public function akreditasi(){
        $admin = Admin::where('id',1)->get();
        foreach($admin as $item)
        {
            $admin_email = $item->email;
            $admin_phone = $item->no_phone;
        }
        $logo = Logo::get();
        foreach($logo as $item ) {
            $logo_school = $item->logo;
        }
        $akreditasi = Akreditasi::get();
        foreach($akreditasi as $item){
            $detail = $item->detail_akreditasi;
        }
        return view('front.profile.akreditasi', compact('admin_email','admin_phone','logo_school','detail'));
    }

    public function sejarah(){
        $admin = Admin::where('id',1)->get();
        foreach($admin as $item)
        {
            $admin_email = $item->email;
            $admin_phone = $item->no_phone;
        }
        $logo = Logo::get();
        foreach($logo as $item ) {
            $logo_school = $item->logo;
        }
        $sejarah = Sejarah::get();
        foreach($sejarah as $item){
            $detail = $item->sejarah_sekolah;
        }
        return view('front.profile.sejarah', compact('admin_email','admin_phone','logo_school','detail'));
    }

    public function sambtnKepsek(){
        $admin = Admin::where('id',1)->get();
        foreach($admin as $item)
        {
            $admin_email = $item->email;
            $admin_phone = $item->no_phone;
        }
        $logo = Logo::get();
        foreach($logo as $item ) {
            $logo_school = $item->logo;
        }
        //untuk baca data sambutan kepala sekolah berdasarkan id
        $message = Message::get();
        foreach($message as $item){
            $msg_photo = $item->msg_photo;
            $nmKepsek = $item->name;
            $jabatan = $item->position;
            $kalmtSambutan = $item->msg;
        }
        return view('front.profile.sambutan', compact('admin_email','admin_phone','logo_school','msg_photo','nmKepsek','jabatan','kalmtSambutan'));
    }

}
