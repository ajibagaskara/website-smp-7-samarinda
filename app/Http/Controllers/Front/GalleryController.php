<?php

namespace App\Http\Controllers\Front;

use App\Models\Logo;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    public function index()
    {
        $admin = Admin::where('id',1)->get();
        foreach($admin as $item)
        {
            $admin_email = $item->email;
            $admin_phone = $item->no_phone;
        }
        $logo = Logo::get();
        foreach($logo as $item ) {
            $logo_school = $item->logo;
        }
        return view('front.gallery.index', compact('admin_email','admin_phone','logo_school'));
    }
}
