<?php

namespace App\Http\Controllers\Front;

use App\Models\Logo;
use App\Models\Post;
use App\Models\Admin;
use App\Models\Author;
// use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    //detail post method
    public function detail($id)
    {
        $admin = Admin::where('id',1)->get();
        foreach($admin as $item)
        {
            $admin_email = $item->email;
            $admin_phone = $item->no_phone;
        }
        $logo = Logo::get();
        foreach($logo as $item ) {
            $logo_school = $item->logo;
        }
        $post_detail = Post::with('nCategory')->where('id',$id)->first();
        if($post_detail->author_id == 0)
        {
            $user_data = Admin::where('id', $post_detail->admin_id)->first();
            // dd($user_data->name);
        }
        else
        {
            $user_data = Author::where('id',$post_detail->author_id)->first();
        }

        //Update View Page 
        $new_value = $post_detail->visitors+1;
        $post_detail->visitors = $new_value;
        $post_detail->update();
        // dd($post_detail->visitors);

        $related_post_array = Post::where('status_post', '!=', 'Draft')->with('nCategory')->orderBy('id','desc')->where('category_id', $post_detail->category_id)->get();

        return view('front.post.detail', compact('post_detail','user_data','related_post_array','admin_email','admin_phone','logo_school'));
    }

}
