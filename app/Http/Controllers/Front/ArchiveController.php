<?php

namespace App\Http\Controllers\Front;

use App\Models\Logo;
use App\Models\Post;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArchiveController extends Controller
{
    public function show(Request $request)
    {
        $admin = Admin::get();
        foreach($admin as $item)
        {
            $admin_email = $item->email;
            $admin_phone = $item->no_phone;
        }
        $logo = Logo::get();
        foreach($logo as $item ) {
            $logo_school = $item->logo;
        }
        // echo $request->archive_month_year;
        $temp = explode('-', $request->archive_month_year);
        $month = $temp[0];
        $year = $temp[1];
        $post_data_archive = Post::with('nCategory')->whereMonth('created_at', '=', $month)->whereYear('created_at', '=', $year)->get();

        foreach($post_data_archive as $item)
        {
            $date_arc = strtotime($item->created_at);
            $updated_date = date('F, Y', $date_arc);
            break;
        }

        // dd($post_data_archive);
        return view('front.archive.index',compact('post_data_archive', 'updated_date', 'admin_email', 'admin_phone','logo_school'));
    }
}
