<?php

namespace App\Http\Controllers\Front;

use App\Models\Logo;
use App\Models\Post;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function index()
    {
        $admin = Admin::where('id',1)->get();
        foreach($admin as $item)
        {
            $admin_email = $item->email;
            $admin_phone = $item->no_phone;
        }
        $logo = Logo::get();
        foreach($logo as $item ) {
            $logo_school = $item->logo;
        }

        //batasi berita yang ditampilkan, hanya berita dengan status Penting atau Umum
        $post_data = Post::where('status_post', '!=', 'Draft')->with('nCategory')->orderBy('id','desc')->paginate(10);

        return view('front.news.index', compact('post_data','admin_email','admin_phone','logo_school'));
    }
}
