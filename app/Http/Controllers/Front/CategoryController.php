<?php

namespace App\Http\Controllers\Front;

use App\Models\Logo;
use App\Models\Post;
use App\Models\Admin;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    //
    public function index($id)
    {
        $admin = Admin::where('id',1)->get();
        foreach($admin as $item)
        {
            $admin_email = $item->email;
            $admin_phone = $item->no_phone;
        }
        $logo = Logo::get();
        foreach($logo as $item ) {
            $logo_school = $item->logo;
        }
        $category_posts = Category::where('id', $id)->first();
        $post_data = Post::where('status_post', '!=', 'Draft')->where('category_id', $id)->orderBy('id', 'desc')->paginate(4);
        // dd($post_data);
        // dd($category_posts);
        return view('front.category.index', compact('category_posts','post_data','admin_email','admin_phone','logo_school'));
    }
}
