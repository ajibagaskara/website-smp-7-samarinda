<?php

namespace App\Http\Controllers\Front;

use App\Models\Logo;
use App\Models\Post;
use App\Models\Admin;
use App\Models\Message;
use App\Models\Setting;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index(){
        //untuk baca data email, dan nomor handphone admin
        $admin = Admin::where('id',1)->get();
        foreach($admin as $item)
        {
            $admin_email = $item->email;
            $admin_phone = $item->no_phone;
        }
        
        //untuk baca logo sekolah
        $logo = Logo::get();
        foreach($logo as $item ) {
            $logo_school = $item->logo;
        }
        //untuk baca data sambutan kepala sekolah berdasarkan id
        $message = Message::where('id',1)->first();
        //untuk baca data pengaturan pengumuman berdasarkan id
        $setting = Setting::where('id',1)->first();
        //untuk baca data post berdasarkan statusnya 
        $posts = Post::get()->where('status_post', 'Penting');
        //untuk baca data post berdasarkan statusnya 
        $post_slider = Post::with('nCategory')->get()->where('show_slider', 'Aktif');
        //untuk baca data post, diurutkan berdasarkan id
        $post_data = Post::where('status_post', '!=', 'Draft')->with('nCategory')->orderBy('id','desc')->get();
        //untuk baca data category yang memiliki post
        $categories = Category::with('mPosts')->get();
        // dd($posts);
        // dd($admin);
        return view('front.home', compact('setting','posts', 'categories', 'post_data','admin_email','admin_phone','message','post_slider','logo_school'));
        
    }
}
