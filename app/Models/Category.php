<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Post;

class Category extends Model
{
    use HasFactory;

    //mPosts = many Posts
    public function mPosts()
    {   
        return $this->hasMany(Post::class)->orderBy('id','desc');
    }
}
