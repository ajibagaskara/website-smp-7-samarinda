<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\Front\NewsController;
use App\Http\Controllers\Front\PostController;
use App\Http\Controllers\Admin\AuthorController;
use App\Http\Controllers\Front\ArchiveController;
use App\Http\Controllers\Front\GalleryController;
use App\Http\Controllers\Front\CategoryController;
use App\Http\Controllers\Admin\AdminHomeController;
use App\Http\Controllers\Admin\AdminLinkController;
use App\Http\Controllers\Admin\AdminPostController;
use App\Http\Controllers\Admin\AdminLoginController;
use App\Http\Controllers\Admin\AuthorPostController;
use App\Http\Controllers\Admin\AdminProfileController;
use App\Http\Controllers\Admin\AdminSejarahController;
use App\Http\Controllers\Admin\AdminSettingController;
use App\Http\Controllers\Admin\AdminCategoryController;
use App\Http\Controllers\Admin\AdminStrukturController;
use App\Http\Controllers\Admin\AdminVisiMisiController;
use App\Http\Controllers\Front\ProfileSchoolController;
use App\Http\Controllers\Admin\AdminAkreditasiController;

// Start Front Page Route
Route::get('/', [HomeController::class, 'index'])->name('home');
// Finish Front Page Route

// Front Post Route 
Route::get('/post-detail/{id}', [PostController::class, 'detail'])->name('post-detail');
// Finish Post Route

//Berita Menu Route
Route::get('/news/all-list', [NewsController::class, 'index'])->name('news');

//Galeri Menu Route
Route::get('/gallery/all-gallery', [GalleryController::class, 'index'])->name('gallery');

//Profil Menu Route
Route::get('/profil/visi-misi', [ProfileSchoolController::class, 'visiMisi'])->name('profil.visi-misi');

//Strutur Menu Route
Route::get('/profil/struktur', [ProfileSchoolController::class, 'struktur'])->name('profil.struk');

//Akreditasi Menu Route
Route::get('/profil/akreditasi', [ProfileSchoolController::class, 'akreditasi'])->name('profil.akreditasi');

//Akreditasi Menu Route
Route::get('/profil/sejarah', [ProfileSchoolController::class, 'sejarah'])->name('profil.sejarah');

//Sambutan Kepala Sekolah Menu Route
Route::get('/profil/sambutan-kepsek', [ProfileSchoolController::class, 'sambtnKepsek'])->name('profil.sambutan-kepsek');

//Category Route
Route::get('/category-post/{id}', [CategoryController::class, 'index'])->name('category.post');

//Archive Route
Route::post('/archive/show', [ArchiveController::class, 'show'])->name('archive.show');



// Start Login Admin Route
Route::get('/admin/login', [AdminLoginController::class, 'login'])->name('admin.login');
Route::post('/admin/login-submit', [AdminLoginController::class, 'loginSubmit'])->name('admin.login.submit');
// Finish Front Page Route

// Start Forget Pass Route
Route::get('/admin/forget-pass', [AdminLoginController::class, 'forget'])->name('admin.forget');
Route::post('/admin/forget-pass-submit', [AdminLoginController::class, 'forgetPassword'])->name('forget.password');
// Finish Forget Pass Route

// Start Home Page Route
Route::get('/admin/home', [AdminHomeController::class, 'index'])->name('admin.home')->middleware('admin:admin');
Route::get('/admin/logout', [AdminLoginController::class, 'logout'])->name('admin.logout');
Route::get('/admin/edit-profile', [AdminProfileController::class, 'index'])->name('admin.edit-profile')->middleware('admin:admin');
Route::post('/admin/edit-profile-submit', [AdminProfileController::class, 'profileSubmit'])->name('admin.profile.submit');
// Finish Home Page Route


// Start Category Route
Route::get('/admin/category/create', [AdminCategoryController::class, 'create'])->name('category.create')->middleware('admin:admin');
Route::get('/admin/category/show', [AdminCategoryController::class, 'show'])->name('category.show')->middleware('admin:admin');
Route::post('/admin/category/store', [AdminCategoryController::class, 'store'])->name('category.store');
Route::get('/admin/category/edit/{id}', [AdminCategoryController::class, 'edit'])->name('category.edit')->middleware('admin:admin');
Route::post('/admin/category/update/{id}', [AdminCategoryController::class, 'update'])->name('category.update');
Route::get('/admin/category/delete/{id}', [AdminCategoryController::class, 'delete']);
// Finish Category Route

// Start Post Route
Route::get('/admin/post/create', [AdminPostController::class, 'create'])->name('post.create')->middleware('admin:admin');
Route::get('/admin/post/show', [AdminPostController::class, 'show'])->name('post.show')->middleware('admin:admin');
Route::post('/admin/post/store', [AdminPostController::class, 'store'])->name('post.store');
Route::get('/admin/post/edit/{id}', [AdminPostController::class, 'edit'])->name('post.edit')->middleware('admin:admin');
Route::post('/admin/post/update/{id}', [AdminPostController::class, 'update'])->name('post.update');
Route::get('/admin/post/delete/{id}', [AdminPostController::class, 'delete']);
//Finish post Route

//Setting Route
Route::get('/admin/setting', [AdminSettingController::class, 'index'])->name('setting.index')->middleware('admin:admin');
Route::post('/admin/setting/update', [AdminSettingController::class, 'update'])->name('setting.update');
Route::post('/admin/message/update', [AdminSettingController::class, 'updateMessage'])->name('setting.message');
Route::post('/admin/logo/update', [AdminSettingController::class, 'updateLogo'])->name('setting.logo');
//Finish Setting Route

// Start Manage Author Route
Route::get('/admin/author/show', [AuthorController::class, 'show_author'])->name('author.show')->middleware('admin:admin');
Route::get('/admin/author/create', [AuthorController::class, 'create'])->name('author.create')->middleware('admin:admin');
Route::post('/admin/author/store', [AuthorController::class, 'store'])->name('author.store');
Route::get('/admin/author/edit/{id}', [AuthorController::class, 'edit'])->name('author.edit')->middleware('admin:admin');
Route::post('/admin/author/update/{id}', [AuthorController::class, 'update'])->name('author.update');
Route::get('/admin/author/delete/{id}', [AuthorController::class, 'delete'])->name('author.delete');


//Start Author Route
Route::get('/author/login', [AuthorController::class, 'login'])->name('author.login');
Route::post('/author/login-submit', [AuthorController::class, 'loginSubmit'])->name('author.login.submit');
Route::get('/author/logout', [AuthorController::class, 'logout'])->name('author.logout');
Route::get('/author/home', [AuthorController::class, 'index'])->name('author.home')->middleware('author:author');
//Stop Author Route


//Author's Post Route
Route::get('/author/post/show', [AuthorPostController::class, 'show_post'])->name('author.post.show')->middleware('author:author');
Route::get('/author/post/create', [AuthorPostController::class, 'create'])->name('author.post.create')->middleware('author:author');
Route::post('/author/post/store', [AuthorPostController::class, 'store'])->name('author.post.store');
Route::get('/author/post/edit/{id}', [AuthorPostController::class, 'edit'])->name('author.post.edit')->middleware('author:author');
Route::post('/author/post/update/{id}', [AuthorPostController::class, 'update'])->name('author.post.update');
Route::get('/author/post/delete/{id}', [AuthorPostController::class, 'delete']);
//End Route

// Manage Profile (Visi Misi)
Route::get('/admin/visi-misi', [AdminVisiMisiController::class, 'index'])->name('profile.visi')->middleware('admin:admin');
Route::post('/admin/visi-misi/update', [AdminVisiMisiController::class, 'updateVisiMisi'])->name('visi.update');

//Manage Profile (Struktur)
Route::get('/admin/struktur', [AdminStrukturController::class, 'index'])->name('profile.struktur')->middleware('admin:admin');
Route::post('/admin/struktur/update', [AdminStrukturController::class, 'updateStruktur'])->name('struktur.update');

//Manage Profile (Akreditasi)
Route::get('/admin/akreditasi', [AdminAkreditasiController::class, 'index'])->name('profile.akreditasi')->middleware('admin:admin');
Route::post('/admin/akreditasi/update', [AdminAkreditasiController::class, 'updateAkreditasi'])->name('akreditasi.update');

//Manage Profile (Sejarah)
Route::get('/admin/sejarah', [AdminSejarahController::class, 'index'])->name('profile.sejarah')->middleware('admin:admin');
Route::post('/admin/sejarah/update', [AdminSejarahController::class, 'updateSejarah'])->name('sejarah.update');





